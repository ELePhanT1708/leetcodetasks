string_1 = "au"


def lengthOfLongestSubstring(s: str) -> int:
    substring = ''
    length_max = 0
    _length = 0
    for letter in s:
        print(letter)
        if letter in substring:
            if _length > length_max:
                length_max = _length
                substring = letter
                _length = 1

        else:
            substring += letter
            _length += 1
            print(f'Substring = {substring}, \nLength - {_length} ')
            if _length > length_max:
                length_max = _length
    return length_max

def main():
    print(lengthOfLongestSubstring(string_1))
    return 1



if __name__ == '__main__':
    main()