from collections import Counter
from time import process_time_ns

def do_twice(function):
    def wrapper2(*args, **kwargs):
        function(*args, **kwargs)
        function(*args, **kwargs)
        # return function(*args, **kwargs)
    return wrapper2


def time_calculate(function) -> int:
    def wrapper(*args, **kwargs):
        time_init = process_time_ns()
        function(*args, **kwargs)
        time_end = process_time_ns()
        print(f'Time for processing = {time_end - time_init} ns\n {function.__name__}')
        return function(*args, **kwargs)
    return wrapper

@do_twice
@time_calculate
def canConstruct(ransomNote: str, magazine: str) -> bool:
    curr_check = magazine

    for letter in ransomNote:
        if letter in curr_check:
            curr_check = curr_check.replace(letter, '', 1)
        else:
            return False
    return True

@do_twice
@time_calculate
def canConstruct_collection(ransomNote: str, magazine: str) -> bool:
    _in = Counter(ransomNote)
    _check = Counter(magazine)
    return _in <= _check

if __name__ == '__main__':
    print(canConstruct('shop', 'pos'))
    print(canConstruct_collection('shop', 'pos'))



