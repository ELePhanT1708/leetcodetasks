from typing import List

def pivotIndex(nums: List[int]) -> int:
    array_sum_left = []
    _sum = 0
    for value in nums:
        _sum += value
        array_sum_left.append(_sum - value)
    for index, summ in enumerate(array_sum_left):
        print(f'{index } : {summ}')
    print('\n')
    right_sum = []
    for index,value in enumerate(nums):
        right_sum.append(array_sum_left[-1] + nums[-1] - array_sum_left[index] - value)
    for index, summ in enumerate(right_sum):
        print(f'{index } : {summ}')
    for value1, value2 in zip(enumerate(array_sum_left), enumerate(right_sum)):
        if value1 == value2 :
            return value1[0]
        return -1

def pivotWeight(nums: List[int]) -> int:
    left , right = 0, sum(nums)
    for index, current_weight in enumerate(nums):
        right -= current_weight
        if left == right :
            return index
        left += current_weight
    return -1



if __name__== '__main__':
    print(pivotIndex([2, 1, -1]))
    print(pivotWeight([2, 1, -1]))